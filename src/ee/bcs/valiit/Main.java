package ee.bcs.valiit;

public class Main {

    public static void main(String[] args) {
        Dog koer1 = new Dog("Muri");
        // koer1.setName("Muri");
        koer1.bark();
        Dog koer2 = new Dog();
        koer2.setName("Muki");
        koer2.bark();
		Dog koer3 = new Dog ("Tommi");
		koer3.bark();
    }
}
