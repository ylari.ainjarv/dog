package ee.bcs.valiit;

public class Dog {

    public Dog() {}

    public Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public void bark() {
        System.out.println(name
                + " tegi Auh-Auh!");
    }
}
